package cmd

import (
	"github.com/spf13/cobra"
	"time"
	"bitbucket.org/kindlychung/splitimagedataset/lib"
	"math/rand"
)

var inputFolder string
var outputFolder string
var trainRatio float64
var validateRatio float64

var PartitionCommand = &cobra.Command{
	Use:   "partition",
	Short: "",
	Long:  ``,
	Args:  cobra.MinimumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		rand.Seed(time.Now().Unix())
		splitter := lib.NewFolderSplitter(
			inputFolder,
			outputFolder,
			make([]string, 0),
			trainRatio, validateRatio,
		)
		splitter.Partition()
	},
}


func init() {
	PartitionCommand.Flags().StringVarP(&inputFolder, "inputFolder", "i", "/tmp", "Folder that contains input data, each class has one subfolder")
	PartitionCommand.Flags().StringVarP(&outputFolder, "outputFolder", "o", "/tmp", "Folder that will contain partitioned data")
	PartitionCommand.Flags().Float64VarP(&trainRatio, "trainRatio", "t", .7, "Ratio of training set")
	PartitionCommand.Flags().Float64VarP(&validateRatio, "validateRatio", "v", .7, "Ratio of validating set")
}
